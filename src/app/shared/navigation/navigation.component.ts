import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

    auth:boolean;
    idName:string = "Alain";
    idUser:string = "22";

    constructor() { }

    ngOnInit(): void {
        this.checkLog();
    }

    checkLog() {
        if (localStorage.getItem('myId') != null) {
            this.auth = true;
        } else {
            this.auth = false;
        }
    }
    
    goConnect() {
        localStorage.setItem('myId', this.idUser);
        localStorage.setItem('idName', this.idName);
        this.auth = true;
    }
    
    goDisconnect() {
        localStorage.removeItem('myId');
        localStorage.removeItem('idName');
        this.auth = false;
    }
}
